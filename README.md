comment faire un site pigeon ?

1- utiliser [strapi](https://docs.strapi.io/dev-docs/intro)  et mettez 

<!-- npx create-strapi-app@latest nom-de-proj -->

pour lancer strapi c'est 

<!-- npm run develop -->

si ton Node est pas bon car la version est pas la bonne, fait <!-- "nvm use XX.X "(en l'occurence Node 18.0) -->

(adresse mail : celle de ton vieux pseudo et le mdp tu le connais) 

ensuite tu fais le content type builder, tu as un + en dessous, tu nomme le truc genre "Produit " tu fais continue, après tu rajoute des lignes et leurs valeurs (nom en text etc...)

lancer postman, tester la requête avec (/api/:pluralApiId/:documentId) voir adresse de la page ou tu crée pour le localhost + le component + l'id de ton truc donc (http://localhost:1337/api/produits/:id) et tu met dans "path variables" la value qui t'intéresse (1 en l'occurence)

ensuite, aller dans settings > users & perms > roles > Public, tu joue avec les cases pour les permissions et quand tu clique sur l'écrou machin t'as la route pour Postman

OUBLIE PAS DE SAVE STRAPI POUR FAIRE LES TESTS ANDOUILLE

questions à se poser à CHAQUE FOIS que tu crée un produit:

- est-ce que j'ai besoin du draft/publish ?
- est-ce que j'ai des contraintes sur les types des attributs ? (nom unique, prix non inférieur à zéro)

pour le front :

pour lancer le front avec 11ty dans ton terminal front :

<!-- npx @11ty/eleventy --serve -->

et tu utilise le localhost qu'ils te filent

faire un fetch pour récup la data dans ta page html crée par tes soins, tu fais la balise script à l'arrache et fout ça dedans 

<script>
    let megaprod
    fetch('http://localhost:1337/api/produits/1')
    .then(response => response.json())
    .then(JsonData => {megaprod = JsonData
    console.log(megaprod)
    })
</script>

pour trouver ça dans la console de la page, tu fais l'inspect de la page, tu regarde dans "journaux " et tu fouilles un peu t'es un grand garçon


POUR RELANCER LE SERVEUR :

va dans ton back, le fichier "package.json" et tu vas dans ton terminal back pour taper " npm run develop " CA RELANCE STRAPI !!!! (le back)

de là garde ton seum car tu vas réutiliser des composants déjà créées, utilise Shoelace c'est la plus simple : [shoelace](https://shoelace.style/)

 tu fais le copier-coller du link et script :

    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@shoelace-style/shoelace@2.4.0/dist/themes/light.css" />
    <script type="module" src="https://cdn.jsdelivr.net/npm/@shoelace-style/shoelace@2.4.0/dist/shoelace-autoloader.js"></script>

après t'arrange comme tu veux etc en fouillant la doc, des copypaste etc et tu t'amuse avec le script et le html

faire attention à :
1- l'id des élements
2- les css
3- les includes et leur ordre sur la page

récup une commande d'un produit client : 
<!-- http://localhost:1337/api/commandes/1?populate[ligne_commandes][populate]&populate[client]=* -->